SVZM
====

![A demonstration of SVZM.](https://i.postimg.cc/cLtQLjGP/image.png)

![Pipeline status badge](https://gitlab.com/slondr/svzm/badges/master/pipeline.svg)

SVZM (the **Sv**elte **Z**-**M**achine) is a functional Z-Machine interpreter and interactive ficion player. SVZM is entirely client-side, and thus works on all platforms that feature a functional web browser and support for relatively modern JavaScript.

And the best part is, SVZM doesn't use JQuery!

Right now, SVZM only support z3-format games. To play them, just drag and drop the z3 file into the terminal window. 

Games should be playable from beginning to end; so far, save and restore have not been implemented. If you find that a z3 game which loads successfully but breaks before you can complete it for some reason, please flag a new issue in this repo.

[Try it now!](https://svzm.ericlondres.tech/)

## Technical Details

SVZM is written entirely in JavaScript. The Z-code interpreter is JSZM, authored by [zzo38](http://zzo38computer.org/jszm/jszm.js). JSZM is public-domain code. The front end and connection layer is written by me, and is licensed under the terms of the GNU GPLv3. 

[Svelte](svelte.dev) is used mainly as a convenient wrapper for Rollup and to keep the directory organized. The majority of the logic is contained in `Terminal.svelte`.
